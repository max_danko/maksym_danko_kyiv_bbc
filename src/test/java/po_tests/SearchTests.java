package po_tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SearchTests extends BaseTest {

    private static final String TOP_SEARCHED_TITLE = "Monkeypox cases investigated in Europe, US, Canada and Australia";

    @Test
    public void checkSearchResultForCategoryHeadlineNewsArticle() {
        getHomePage().goToNewsPage(TIMER);
        String headlineArticleCategoryLinkText = getNewsPage().getHeadlineArticleCategory();

        getNewsPage().goToSearchPage(TIMER);

        getSearchPage().makeSearch(TIMER, headlineArticleCategoryLinkText);
        assertEquals(TOP_SEARCHED_TITLE, getSearchPage().getFirstSearchResultsTitleText(TIMER));
    }
}
