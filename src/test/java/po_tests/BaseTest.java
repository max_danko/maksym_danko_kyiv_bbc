package po_tests;

import pages.main.HomePage;
import pages.main.SearchPage;
import pages.news.AskQuestionNewsPage;
import pages.news.CoronavirusNewsPage;
import pages.news.HaveYourSayNewsPage;
import pages.news.MainNewsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class BaseTest {
    private WebDriver driver;
    private static final String BBC_URL = "https://www.bbc.com/";
    static final long TIMER = 30;

    @BeforeTest
    public void profileSetUp() {
        chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(BBC_URL);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public MainNewsPage getNewsPage() {
        return new MainNewsPage(getDriver());
    }

    public CoronavirusNewsPage getCoronavirusNewsPage() { return new CoronavirusNewsPage(getDriver()); }

    public HaveYourSayNewsPage getHaveYourSayNewsPage() { return new HaveYourSayNewsPage(getDriver()); }

    public AskQuestionNewsPage getAskQuestionNewsPage() { return new AskQuestionNewsPage(getDriver()); }

    public SearchPage getSearchPage() {
        return new SearchPage(getDriver());
    }

}
