package po_tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class NewsTests extends BaseTest {

    private static final String TOP_NEW_TITLE = "Texas school gunman 'walked in unobstructed'";
    private static final List<String> SECONDARY_NEWS_TITLES = Arrays.asList(
            "The teachers who died trying to save their pupils",
            "Shelling in Kharkiv kills eight including baby",
            "Amber Heard: It's easy to forget I'm a human being",
            "Goodfellas star Ray Liotta dies aged 67",
            "Ancient DNA reveals secrets of Pompeii victims");

    @Test
    public void checkHeadlineNewsArticle() {
        getHomePage().goToNewsPage(TIMER);

        getNewsPage().waitVisibilityOfTopNewsTitle(TIMER);
        assertEquals(TOP_NEW_TITLE, getNewsPage().getTopNewsTitleText());
    }

    @Test
    public void checkSecondaryNewsArticle() {
        getHomePage().goToNewsPage(TIMER);
        List<WebElement> secondaryNewsTitles = getNewsPage().getSecondaryNewsTitles(TIMER);

        for (int i = 0; i < secondaryNewsTitles.size(); i++) {
            assertEquals(SECONDARY_NEWS_TITLES.get(i), secondaryNewsTitles.get(i).getText());
        }
    }
}
