package po_tests;

import org.testng.annotations.Test;
import pages.utils.QuestionFormData;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class QuestionTests extends BaseTest {

    private static final QuestionFormData NO_EMAIL_QUESTION_FORM_DATA = new QuestionFormData(
            "Is coronavirus really deadly?",
            "Maksym",
            "",
            "+380632591717",
            "London",
            "87",
            true
    );

    private static final QuestionFormData NOT_CHECKED_TERMS_OF_SERVICE_QUESTION_FORM_DATA = new QuestionFormData(
            "Is coronavirus really deadly?",
            "Maksym",
            "maxgalante20@gmail.com",
            "+380632591717",
            "London",
            "87",
            false
    );

    private static final QuestionFormData EMPTY_QUESTION_FORM_DATA = new QuestionFormData(
            "",
            "Maksym",
            "maxgalante20@gmail.com",
            "+380632591717",
            "London",
            "87",
            true
    );

    private void goToAskQuestionNewsPage() {
        getHomePage().goToNewsPage(TIMER);
        getNewsPage().goToCoronavirusNewsPage(TIMER);
        getCoronavirusNewsPage().goToHaveYourSayNewsPage(TIMER);
        getHaveYourSayNewsPage().goToAskQuestionNewsPage(TIMER);
    }

    @Test
    public void checkUserIsUnableToSubmitQuestionWithoutEmail() {
        goToAskQuestionNewsPage();
        getAskQuestionNewsPage().closeRegistrationModal(TIMER);

        getAskQuestionNewsPage().submitQuestionForm(TIMER, NO_EMAIL_QUESTION_FORM_DATA);
        assertEquals("Email address can't be blank", getAskQuestionNewsPage().getFormSubmitErrorMsg(TIMER));
    }

    @Test
    public void checkUserIsUnableToSubmitQuestionWithoutTermsAcceptance() {
        goToAskQuestionNewsPage();
        getAskQuestionNewsPage().closeRegistrationModal(TIMER);

        getAskQuestionNewsPage().submitQuestionForm(TIMER, NOT_CHECKED_TERMS_OF_SERVICE_QUESTION_FORM_DATA);
        assertEquals("must be accepted", getAskQuestionNewsPage().getFormSubmitErrorMsg(TIMER));
    }

    @Test
    public void checkUserIsUnableToSubmitQuestionWithEmptyQuestion() {
        goToAskQuestionNewsPage();
        getAskQuestionNewsPage().closeRegistrationModal(TIMER);

        getAskQuestionNewsPage().submitQuestionForm(TIMER, EMPTY_QUESTION_FORM_DATA);
        assertEquals("can't be blank", getAskQuestionNewsPage().getFormSubmitErrorMsg(TIMER));
    }
}
