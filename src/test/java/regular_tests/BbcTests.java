package regular_tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.testng.Assert.*;

public class BbcTests {

    private WebDriver driver;
    private static final String BBC_URL = "https://www.bbc.com/";
    private WebDriverWait wait;

    @BeforeTest
    public void profileSetUp() {
        chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(BBC_URL);
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }


    private void goToNewsPage() {
        String newsLinkXpath = "//nav[contains(@class,'international')]//a[contains(@href, 'news')]";
        WebElement newsLink = driver.findElement(By.xpath(newsLinkXpath));
        wait.until(ExpectedConditions.visibilityOf(newsLink));
        newsLink.click();
    }

    private void goToCoronavirusNewsPage() {
        String newsLinkXpath = "//nav[contains(@class, 'wide')]//a[contains(@href, 'coronavirus')]";
        WebElement newsLink = driver.findElement(By.xpath(newsLinkXpath));
        wait.until(ExpectedConditions.visibilityOf(newsLink));
        newsLink.click();
    }

    private void goToHaveYourSayNewsPage() {
        String newsLinkXpath = "//nav[contains(@class, 'wide')]//a[contains(@href, 'your_say')]";
        WebElement newsLink = driver.findElement(By.xpath(newsLinkXpath));
        wait.until(ExpectedConditions.visibilityOf(newsLink));
        newsLink.click();
    }


    private void goToQuestionsNewsPage() {
        String newsLinkXpath = "//h3[contains(text(), 'What questions do you have?')]//ancestor::a";
        WebElement newsLink = driver.findElement(By.xpath(newsLinkXpath));
        wait.until(ExpectedConditions.visibilityOf(newsLink));
        newsLink.click();
    }

    @Test
    public void checkHeadlineNewsArticle() {
        goToNewsPage();

        String topNewsTitleXpath = "//div[contains(@class, 'inline-block@m')]//h3";
        WebElement headlineNewsTitle = driver.findElement(By.xpath(topNewsTitleXpath));
        wait.until(ExpectedConditions.visibilityOfAllElements(headlineNewsTitle));

        assertEquals("Anger as Texas police alter key details of shooting", headlineNewsTitle.getText());
    }

    @Test
    public void checkSecondaryNewsArticle() {
        goToNewsPage();

        List<String> secondaryNewsTitles = Arrays.asList(
                "Uvalde: How a sunny school day ended in bloodshed",
                "Battle for key road as Russians reach Ukraine city",
                "Drunken No 10 party culture laid bare in report",
                "Ros Atkins on… The Sue Gray report",
                "Kate Moss testifies in Depp-Heard trial");

        String topNewsTitleXpath = "//div[contains(@class, 'top-stories__secondary-item')]//h3";
        List<WebElement> topNewsTitles = driver.findElements(By.xpath(topNewsTitleXpath));
        wait.until(ExpectedConditions.visibilityOf(topNewsTitles.get(0)));

        for (int i = 0; i < topNewsTitles.size(); i++) {
            assertEquals(secondaryNewsTitles.get(i), topNewsTitles.get(i).getText());
        }
    }

    @Test
    public void checkSearchResultForCategoryHeadlineNewsArticle() {
        goToNewsPage();

        String searchLinkXpath = "//a[contains(@class, 'search')]";
        WebElement searchLink = driver.findElement(By.xpath(searchLinkXpath));
        wait.until(ExpectedConditions.visibilityOf(searchLink));

        String headlineArticleCategoryLinkXpath = "//div[contains(@class, 'inline-block@m')]//a[contains(@class, 'section-link')]//span";
        String headlineArticleCategoryLinkText = driver.findElement(By.xpath(headlineArticleCategoryLinkXpath)).getText();

        searchLink.click();

        String searchInputXpath = "//input[@id='search-input']";
        WebElement searchInput = driver.findElement(By.xpath(searchInputXpath));
        wait.until(ExpectedConditions.visibilityOf(searchInput));
        searchInput.sendKeys(headlineArticleCategoryLinkText);

        String submitSearchBtnXpath = "//button[@type='submit']";
        WebElement submitSearchBtn = driver.findElement(By.xpath(submitSearchBtnXpath));
        submitSearchBtn.click();

        String searchResultsHeaderTitlesXpath = "//a[contains(@class, 'ssrcss-1ynlzyd')]//p//span";
        WebElement firstSearchResultHeaderTitle = driver.findElements(By.xpath(searchResultsHeaderTitlesXpath)).get(0);
        wait.until(ExpectedConditions.visibilityOf(firstSearchResultHeaderTitle));

        assertEquals("Monkeypox cases investigated in Europe, US, Canada and Australia", firstSearchResultHeaderTitle.getText());
    }

    @Test
    public void checkUserIsUnableToSubmitQuestionWithoutEmail() {
        goToNewsPage();
        goToCoronavirusNewsPage();
        goToHaveYourSayNewsPage();
        goToQuestionsNewsPage();

        String closeRegistrationModalBtnXpath = "//button[contains(@class, 'close')]";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(closeRegistrationModalBtnXpath)));
        WebElement closeRegistrationModalBtn = driver.findElement(By.xpath(closeRegistrationModalBtnXpath));
        closeRegistrationModalBtn.click();

        String textareaQuestionXpath = "//div[contains(@id, 'hearken')]//textarea";
        WebElement textareaQuestion = driver.findElement(By.xpath(textareaQuestionXpath));
        wait.until(ExpectedConditions.visibilityOf(textareaQuestion));
        textareaQuestion.sendKeys("Is coronavirus really deadly?");

        String nameInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Name']";
        WebElement nameInput = driver.findElement(By.xpath(nameInputXpath));
        nameInput.sendKeys("Maksym");

        String emailInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Email address']";
        WebElement emailInput = driver.findElement(By.xpath(emailInputXpath));

        String phoneInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Contact number']";
        WebElement phoneInput = driver.findElement(By.xpath(phoneInputXpath));
        phoneInput.sendKeys("+380639523270");

        String locationInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Location ']";
        WebElement locationInput = driver.findElement(By.xpath(locationInputXpath));
        locationInput.sendKeys("London");

        String ageInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Age']";
        WebElement ageInput = driver.findElement(By.xpath(ageInputXpath));
        ageInput.sendKeys("87");

        String acceptTermsCheckboxXpath = "//div[contains(@id, 'hearken')]//input[@type='checkbox']";
        WebElement acceptTermsCheckbox = driver.findElement(By.xpath(acceptTermsCheckboxXpath));
        acceptTermsCheckbox.click();

        String submitQuestionBtnXpath = "//div[contains(@id, 'hearken')]//button[text()='Submit']";
        WebElement submitQuestionBtn = driver.findElement(By.xpath(submitQuestionBtnXpath));
        wait.until(ExpectedConditions.visibilityOf(submitQuestionBtn));
        assertTrue(submitQuestionBtn.isDisplayed());
        submitQuestionBtn.click();

        wait.until(ExpectedConditions.elementToBeClickable(submitQuestionBtn));

        String emailErrorMsgXpath = "//div[@class='input-error-message']";
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(emailErrorMsgXpath)));
        WebElement emailErrorMsg = driver.findElement(By.xpath(emailErrorMsgXpath));
        assertEquals("Email address can't be blank", emailErrorMsg.getText());
    }

    @Test
    public void checkUserIsUnableToSubmitQuestionWithoutTermsAcceptance() {
        goToNewsPage();
        goToCoronavirusNewsPage();
        goToHaveYourSayNewsPage();
        goToQuestionsNewsPage();

        String closeRegistrationModalBtnXpath = "//button[contains(@class, 'close')]";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(closeRegistrationModalBtnXpath)));
        WebElement closeRegistrationModalBtn = driver.findElement(By.xpath(closeRegistrationModalBtnXpath));
        closeRegistrationModalBtn.click();

        String textareaQuestionXpath = "//div[contains(@id, 'hearken')]//textarea";
        WebElement textareaQuestion = driver.findElement(By.xpath(textareaQuestionXpath));
        wait.until(ExpectedConditions.visibilityOf(textareaQuestion));
        textareaQuestion.sendKeys("Is coronavirus really deadly?");

        String nameInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Name']";
        WebElement nameInput = driver.findElement(By.xpath(nameInputXpath));
        nameInput.sendKeys("Maksym");

        String emailInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Email address']";
        WebElement emailInput = driver.findElement(By.xpath(emailInputXpath));
        emailInput.sendKeys("maxgalante20@gmail.com");

        String phoneInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Contact number']";
        WebElement phoneInput = driver.findElement(By.xpath(phoneInputXpath));

        String locationInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Location ']";
        WebElement locationInput = driver.findElement(By.xpath(locationInputXpath));
        locationInput.sendKeys("London");

        String ageInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Age']";
        WebElement ageInput = driver.findElement(By.xpath(ageInputXpath));
        ageInput.sendKeys("87");

        String submitQuestionBtnXpath = "//div[contains(@id, 'hearken')]//button[text()='Submit']";
        WebElement submitQuestionBtn = driver.findElement(By.xpath(submitQuestionBtnXpath));
        wait.until(ExpectedConditions.visibilityOf(submitQuestionBtn));
        assertTrue(submitQuestionBtn.isDisplayed());
        submitQuestionBtn.click();

        wait.until(ExpectedConditions.elementToBeClickable(submitQuestionBtn));

        String emailErrorMsgXpath = "//div[@class='input-error-message']";
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(emailErrorMsgXpath)));
        WebElement emailErrorMsg = driver.findElement(By.xpath(emailErrorMsgXpath));
        assertEquals("must be accepted", emailErrorMsg.getText());
    }

    @Test
    public void checkUserIsUnableToSubmitQuestionWithEmptyQuestion() {
        goToNewsPage();
        goToCoronavirusNewsPage();
        goToHaveYourSayNewsPage();
        goToQuestionsNewsPage();

        String closeRegistrationModalBtnXpath = "//button[contains(@class, 'close')]";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(closeRegistrationModalBtnXpath)));
        WebElement closeRegistrationModalBtn = driver.findElement(By.xpath(closeRegistrationModalBtnXpath));
        closeRegistrationModalBtn.click();

        String nameInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Name']";
        WebElement nameInput = driver.findElement(By.xpath(nameInputXpath));
        nameInput.sendKeys("Maksym");

        String emailInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Email address']";
        WebElement emailInput = driver.findElement(By.xpath(emailInputXpath));
        emailInput.sendKeys("maxgalante20@gmail.com");

        String phoneInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Contact number']";
        WebElement phoneInput = driver.findElement(By.xpath(phoneInputXpath));
        phoneInput.sendKeys("+380639523270");

        String locationInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Location ']";
        WebElement locationInput = driver.findElement(By.xpath(locationInputXpath));
        locationInput.sendKeys("London");

        String ageInputXpath = "//div[contains(@id, 'hearken')]//input[@placeholder='Age']";
        WebElement ageInput = driver.findElement(By.xpath(ageInputXpath));
        ageInput.sendKeys("87");

        String acceptTermsCheckboxXpath = "//div[contains(@id, 'hearken')]//input[@type='checkbox']";
        WebElement acceptTermsCheckbox = driver.findElement(By.xpath(acceptTermsCheckboxXpath));
        acceptTermsCheckbox.click();

        String submitQuestionBtnXpath = "//div[contains(@id, 'hearken')]//button[text()='Submit']";
        WebElement submitQuestionBtn = driver.findElement(By.xpath(submitQuestionBtnXpath));
        wait.until(ExpectedConditions.visibilityOf(submitQuestionBtn));
        assertTrue(submitQuestionBtn.isDisplayed());
        submitQuestionBtn.click();

        wait.until(ExpectedConditions.elementToBeClickable(submitQuestionBtn));

        String emailErrorMsgXpath = "//div[@class='input-error-message']";
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(emailErrorMsgXpath)));
        WebElement emailErrorMsg = driver.findElement(By.xpath(emailErrorMsgXpath));
        assertEquals("can't be blank", emailErrorMsg.getText());
    }
}
