package stepdefinitions;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.main.HomePage;
import pages.main.SearchPage;
import pages.news.*;
import pages.sports.FootballSportPage;
import pages.sports.MainSportPage;
import pages.sports.MatchFootballSportPage;
import pages.sports.ScoresFootballSportPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.*;

public class DefinitionSteps {

    private static final long TIMEOUT = 30;
    private String topArticleCategory;

    WebDriver driver;
    PageFactoryManager pageFactoryManager;

    HomePage homePage;
    MainNewsPage mainNewsPage;
    CoronavirusNewsPage coronavirusNewsPage;
    HaveYourSayNewsPage haveYourSayNewsPage;
    AskQuestionNewsPage askQuestionNewsPage;
    SearchPage searchPage;
    QuestionForm questionForm;
    MainSportPage mainSportPage;
    FootballSportPage footballSportPage;
    ScoresFootballSportPage scoresFootballSportPage;
    MatchFootballSportPage matchFootballSportPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Given("User opens {string} page")
    public void openHomePage(String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @When("User clicks News page link")
    public void goToNewsPage() {
        homePage.goToNewsPage(TIMEOUT);
        mainNewsPage = pageFactoryManager.getNewsPage();
    }

    @And("User waits for top news title on News page")
    public void waitForTopNewsTitle() {
        mainNewsPage.waitVisibilityOfTopNewsTitle(TIMEOUT);
    }

    @Then("User checks that top news title is {string}")
    public void checkTopNewsTitleEqualsExpected(String expected) {
        assertEquals(expected, mainNewsPage.getTopNewsTitleText());
    }

    @Then("User checks that secondary news titles are equal to expected titles")
    public void checkSecondaryNewsArticleEqualsExpected() {
        String[] expectedTitlesList = {
                "Zelensky makes first front-line trip to Kharkiv",
                "UK urges investigation into Paris football chaos",
                "French jetpack inventor injured in lake crash",
                "Israeli nationalists march in Jerusalem amid tension",
                "Landslides and floods kill dozens in Brazil"};
        for (int i = 0; i < expectedTitlesList.length; i++) {
            assertEquals(expectedTitlesList[i], mainNewsPage.getSecondaryNewsTitles(TIMEOUT).get(i).getText());
        }
    }

    private static final String TOP_SEARCHED_TITLE = "Monkeypox cases investigated in Europe, US, Canada and Australia";

    @And("User saves top article category from News page")
    public void saveTopArticleCategory() {
        topArticleCategory = mainNewsPage.getHeadlineArticleCategory();
    }

    @And("User clicks on search field")
    public void goToSearchPage() {
        mainNewsPage.goToSearchPage(TIMEOUT);
        searchPage = pageFactoryManager.getSearchPage();
    }

    @And("User makes search by top article category from News page")
    public void makeSearchWithTopArticleCategory() {
        searchPage.makeSearch(TIMEOUT, topArticleCategory);
    }

    @Then("User checks that top news title from search results is {string}")
    public void checkSearchResultByTopNewsCategoryEqualsExpected(String category) {
        assertEquals(category, searchPage.getFirstSearchResultsTitleText(TIMEOUT));
    }

    @And("User clicks Coronavirus link")
    public void goToCoronavirusNewsPage() {
        mainNewsPage.goToCoronavirusNewsPage(TIMEOUT);
        coronavirusNewsPage = pageFactoryManager.getCoronavirusNewsPage();
    }

    @And("User clicks 'Your Coronavirus Stories' tab")
    public void goToHaveYourSayNewsPage() {
        coronavirusNewsPage.goToHaveYourSayNewsPage(TIMEOUT);
        haveYourSayNewsPage = pageFactoryManager.getHaveYourSayNewsPage();
    }

    @And("User clicks 'Your questions answered: What questions do you have?' link")
    public void goToAskQuestionNewsPage() {
        haveYourSayNewsPage.goToAskQuestionNewsPage(TIMEOUT);
        askQuestionNewsPage = pageFactoryManager.getAskQuestionNewsPage();
    }

    @When("Registration modal shows up user clicks on 'Close' button")
    public void clickCloseRegistrationModal() {
        askQuestionNewsPage.closeRegistrationModal(TIMEOUT);
        questionForm = pageFactoryManager.getQuestionForm();
    }

    @And("User enters {string} in 'Question' teaxtarea")
    public void fillInQuestionTextarea(String question) {
        questionForm.fillInTextarea(TIMEOUT, question);
    }

    @And("User enters {string} in 'Name' input field")
    public void fillInNameInputField(String name) {
        questionForm.fillInNameField(TIMEOUT, name);
    }

    @And("User enters {string} in 'Email' input field")
    public void fillInEmailInputField(String email) {
        questionForm.fillInEmailField(TIMEOUT, email);
    }

    @And("User enters {string} in 'Phone' input field")
    public void fillInPhoneInputField(String phone) {
        questionForm.fillInPhoneField(TIMEOUT, phone);
    }

    @And("User enters {string} in 'Location' input field")
    public void fillInLocationInputField(String location) {
        questionForm.fillInLocationField(TIMEOUT, location);
    }

    @And("User enters {string} in 'Age' input field")
    public void fillInAgeInputField(String age) {
        questionForm.fillInAgeField(TIMEOUT, age);
    }

    @And("User marks {string} 'I accept the Terms of Service' checkmark field")
    public void markAcceptanceTermsOfServiceCheckmark(String checkmark) {
        questionForm.markAcceptTermsOfServiceCheckbox(TIMEOUT, checkmark.equals("true"));
    }

    @And("User clicks 'Submit' button")
    public void clickSubmitQuestionButton() {
        questionForm.submitQuestion(TIMEOUT);
    }

    @Then("User checks submission error {string}")
    public void getSubmissionMessageError(String submissionError) {
        assertEquals(submissionError, questionForm.getSubmitFormErrorMsgText(TIMEOUT));
    }

    @When("User clicks Sport page link")
    public void goToSportPage() {
        homePage.goToSportPage(TIMEOUT);
        mainSportPage = pageFactoryManager.getSportPage();
    }

    @And("User clicks Football tab")
    public void goToFootballTab() {
        mainSportPage.clickOnFootballTab(TIMEOUT);
        footballSportPage = pageFactoryManager.getFootballSportPage();
    }

    @And("User clicks 'Scores and Fixtures' link")
    public void goToFootballScoresAndFixtures() {
        footballSportPage.clickOnScoresAndFixturesLink(TIMEOUT);
        scoresFootballSportPage = pageFactoryManager.getScoresFootballSportPage();
    }

    @When("User searching for {string}")
    public void searchForFootballTeamOrCompetition(String searchRequest) {
        scoresFootballSportPage.enterRequestInSearchInputField(TIMEOUT, searchRequest);
        scoresFootballSportPage.clickSearchSubmitButton();
    }

    @And("User selects {string}")
    public void selectCompetitionMonth(String month) {
        scoresFootballSportPage.selectMonth(month);
    }

    @Then("User checks that {string} vs {string} have played with score {string} : {string}")
    public void checkGameScoreEqualsExpected(String homeTeam, String guestTeam, String homeTeamScore, String guestTeamScore) {
        scoresFootballSportPage.waitForGameListVisibility(TIMEOUT);
        int gameListIndex = scoresFootballSportPage.getIndexOfGameInList(homeTeam, guestTeam);
        assertEquals(homeTeamScore, scoresFootballSportPage.getHomeTeamScore(gameListIndex));
        assertEquals(guestTeamScore, scoresFootballSportPage.getGuestTeamScore(gameListIndex));
    }

    @When("User clicks on {string} vs {string} match")
    public void clickOnGame(String homeTeam, String guestTeam) {
        int gameListIndex = scoresFootballSportPage.getIndexOfGameInList(homeTeam, guestTeam);
        scoresFootballSportPage.clickMatchLink(gameListIndex);
        matchFootballSportPage = pageFactoryManager.getMatchFootballSportPage();
    }

    @Then("User checks page has same teams {string} vs {string} with same score {string} : {string}")
    public void checkGamePageHasSameScoreAsScoresPage(String homeTeam, String guestTeam, String homeTeamScore, String guestTeamScore) {
        matchFootballSportPage.waitVisibilityOfMatchOverview(TIMEOUT);
        assertEquals(homeTeam, matchFootballSportPage.getHomeTeamName());
        assertEquals(guestTeam, matchFootballSportPage.getGuestTeamName());
        assertEquals(homeTeamScore, matchFootballSportPage.getHomeTeamScore());
        assertEquals(guestTeamScore, matchFootballSportPage.getGuestTeamScore());
    }

}
