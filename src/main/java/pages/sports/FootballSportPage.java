package pages.sports;

import org.openqa.selenium.WebDriver;
import pages.main.BasePage;

public class FootballSportPage extends BasePage {

    private static final String SCORES_AND_FIXTURES_LINK = "//ul[@id='sp-nav-secondary']//a[contains(@href, 'scores-fixtures')]";


    public FootballSportPage(WebDriver driver) {
        super(driver);
    }


    public void clickOnScoresAndFixturesLink(long timer) {
        waitVisibilityOfElement(timer, SCORES_AND_FIXTURES_LINK);
        findElementByXpath(SCORES_AND_FIXTURES_LINK).click();
    }


}
