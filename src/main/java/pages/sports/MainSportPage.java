package pages.sports;

import org.openqa.selenium.WebDriver;
import pages.main.BasePage;

public class MainSportPage extends BasePage {

    private static final String FOOTBALL_TAB = "//div[@id='product-navigation-menu']//span[text()='Football']//ancestor::a[contains(@href, 'football')]";


    public MainSportPage(WebDriver driver) {
        super(driver);
    }


    public void clickOnFootballTab(long timer) {
        waitVisibilityOfElement(timer, FOOTBALL_TAB);
        findElementByXpath(FOOTBALL_TAB).click();
    }

}
