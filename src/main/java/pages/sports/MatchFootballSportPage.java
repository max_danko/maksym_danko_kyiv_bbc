package pages.sports;

import org.openqa.selenium.WebDriver;
import pages.main.BasePage;

public class MatchFootballSportPage extends BasePage {

    private static final String MATCH_OVERVIEW = "//div[contains(@class,'match-overview')]";

    private static final String HOME_TEAM_NAME = " //div[contains(@class,'match-overview')]//span[contains(@class, 'home')]//span[contains(@class, 'full')]";
    private static final String GUEST_TEAM_NAME = "//div[contains(@class,'match-overview')]//span[contains(@class, 'away')]//span[contains(@class, 'full')]";
    private static final String HOME_TEAM_SCORE = "//div[contains(@class,'match-overview')]//span[contains(@class, 'number--home')]";
    private static final String GUEST_TEAM_SCORE= "//div[contains(@class,'match-overview')]//span[contains(@class, 'number--away')]";


    public MatchFootballSportPage(WebDriver driver) {
        super(driver);
    }


    public void waitVisibilityOfMatchOverview(long timer) {
        waitVisibilityOfElement(timer, MATCH_OVERVIEW);
    }

    public String getHomeTeamName() {
        return findElementByXpath(HOME_TEAM_NAME).getText();
    }

    public String getGuestTeamName() {
        return findElementByXpath(GUEST_TEAM_NAME).getText();
    }

    public String getHomeTeamScore() {
        return findElementByXpath(HOME_TEAM_SCORE).getText();
    }

    public String getGuestTeamScore() {
        return findElementByXpath(GUEST_TEAM_SCORE).getText();
    }

}
