package pages.sports;

import org.openqa.selenium.WebDriver;
import pages.main.BasePage;

public class ScoresFootballSportPage extends BasePage {

    private static final String TEAM_OR_COMPETITION_SEARCH_FIELD = "//input[@name='search']";
    private static final String SEARCH_SUBMIT_BUTTON = "//ul[@id='search-results-list']//a"; //form[contains(@id, 'search')]//button[@type='submit']
    private static final String HOME_TEAM_NAME_LIST = "//span[contains(@class, 'home')]//span[contains(@class, 'full-team-name')]";
    private static final String GUEST_TEAM_NAME_LIST = "//span[contains(@class, 'away')]//span[contains(@class, 'full-team-name')]";
    private static final String HOME_TEAM_SCORE_LIST = "//span[contains(@class, 'number--home')]";
    private static final String GUEST_TEAM_SCORE_LIST = "//span[contains(@class, 'number--away')]";
    private static final String MATCH_LINK_LIST = "//a[@data-istats-container='match_link']";


    public ScoresFootballSportPage(WebDriver driver) {
        super(driver);
    }


    public void waitForGameListVisibility(long timer) {
        waitVisibilityOfElements(timer, HOME_TEAM_NAME_LIST);
    }

    public void enterRequestInSearchInputField(long timer, String searchRequest) {
        waitVisibilityOfElement(timer, TEAM_OR_COMPETITION_SEARCH_FIELD);
        findElementByXpath(TEAM_OR_COMPETITION_SEARCH_FIELD).sendKeys(searchRequest);
    }

    public void clickSearchSubmitButton() {
        findElementByXpath(SEARCH_SUBMIT_BUTTON).click();
    }

    public void selectMonth(String month) {
        findElementByXpath(String.format("//ul[contains(@id, 'timeline-past')]//span[text()='%s']//ancestor::a", month)).click();
    }

    public int getIndexOfGameInList(String homeTeam, String guestTeam) {
        for (int i = 0; i < HOME_TEAM_NAME_LIST.length(); i++) {
            if (findElementsByXpath(HOME_TEAM_NAME_LIST).get(i).getText().equals(homeTeam) &&
                    findElementsByXpath(GUEST_TEAM_NAME_LIST).get(i).getText().equals(guestTeam)) {
                return i;
            }
        }
        return -1;
    }

    public String getHomeTeamScore(int gameListIndex) {
        return findElementsByXpath(HOME_TEAM_SCORE_LIST).get(gameListIndex).getText();
    }

    public String getGuestTeamScore(int gameListIndex) {
        return findElementsByXpath(GUEST_TEAM_SCORE_LIST).get(gameListIndex).getText();
    }

    public void clickMatchLink(int gameListIndex) {
        findElementsByXpath(MATCH_LINK_LIST).get(gameListIndex).click();
    }
}
