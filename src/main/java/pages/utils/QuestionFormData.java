package pages.utils;

public class QuestionFormData {

    private final String question;
    private final String name;
    private final String email;
    private final String phone;
    private final String location;
    private final String age;
    private final boolean mark;

    public QuestionFormData(String question, String name, String email, String phone, String location, String age, boolean mark) {
        this.question = question;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.location = location;
        this.age = age;
        this.mark = mark;
    }

    public String getQuestion() {
        return question;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getLocation() {
        return location;
    }

    public String getAge() {
        return age;
    }

    public boolean isMark() {
        return mark;
    }
}
