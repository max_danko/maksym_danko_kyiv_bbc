package pages.news;

import pages.main.BasePage;
import org.openqa.selenium.WebDriver;

public class HaveYourSayNewsPage extends BasePage {

    private static final String ASK_QUESTION_NEWS_LINK = "//h3[contains(text(), 'What questions do you have?')]//ancestor::a";


    public HaveYourSayNewsPage(WebDriver driver) {
        super(driver);
    }


    public void goToAskQuestionNewsPage(long timer) {
        waitVisibilityOfElement(timer, ASK_QUESTION_NEWS_LINK);
        findElementByXpath(ASK_QUESTION_NEWS_LINK).click();
    }
}
