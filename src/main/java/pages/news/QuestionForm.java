package pages.news;

import pages.main.BasePage;
import org.openqa.selenium.WebDriver;

public class QuestionForm extends BasePage {

    private static final String TEXTAREA_FOR_QUESTION = "//div[contains(@id, 'hearken')]//textarea";
    private static final String NAME_INPUT = "//div[contains(@id, 'hearken')]//input[@placeholder='Name']";
    private static final String EMAIL_INPUT = "//div[contains(@id, 'hearken')]//input[@placeholder='Email address']";
    private static final String PHONE_INPUT = "//div[contains(@id, 'hearken')]//input[@placeholder='Contact number']";
    private static final String LOCATION_INPUT = "//div[contains(@id, 'hearken')]//input[@placeholder='Location ']";
    private static final String AGE_INPUT = "//div[contains(@id, 'hearken')]//input[@placeholder='Age']";
    private static final String ACCEPT_TERMS_OF_SERVICE_CHECKBOX = "//div[contains(@id, 'hearken')]//input[@type='checkbox']";
    private static final String SUBMIT_QUESTION_BTN = "//div[contains(@id, 'hearken')]//button[text()='Submit']";
    private static final String EMAIL_ERROR_MSG = "//div[@class='input-error-message']";


    public QuestionForm(WebDriver driver) { super(driver); }


    public String getSubmitFormErrorMsgText(long timer) {
        waitVisibilityOfElement(timer, EMAIL_ERROR_MSG);
        return findElementByXpath(EMAIL_ERROR_MSG).getText();
    }

    public void fillInTextarea(long timer, String question) {
        waitVisibilityOfElement(timer, TEXTAREA_FOR_QUESTION);
        findElementByXpath(TEXTAREA_FOR_QUESTION).sendKeys(question);
    }

    public void fillInNameField(long timer, String name) {
        waitVisibilityOfElement(timer, NAME_INPUT);
        findElementByXpath(NAME_INPUT).sendKeys(name);
    }

    public void fillInEmailField(long timer, String email) {
        waitVisibilityOfElement(timer, EMAIL_INPUT);
        findElementByXpath(EMAIL_INPUT).sendKeys(email);
    }

    public void fillInPhoneField(long timer, String phone) {
        waitVisibilityOfElement(timer, PHONE_INPUT);
        findElementByXpath(PHONE_INPUT).sendKeys(phone);
    }

    public void fillInLocationField(long timer, String location) {
        waitVisibilityOfElement(timer, LOCATION_INPUT);
        findElementByXpath(LOCATION_INPUT).sendKeys(location);
    }

    public void fillInAgeField(long timer, String age) {
        waitVisibilityOfElement(timer, AGE_INPUT);
        findElementByXpath(AGE_INPUT).sendKeys(age);
    }

    public void markAcceptTermsOfServiceCheckbox(long timer, boolean mark) {
        waitVisibilityOfElement(timer, ACCEPT_TERMS_OF_SERVICE_CHECKBOX);
        if (mark) findElementByXpath(ACCEPT_TERMS_OF_SERVICE_CHECKBOX).click();
    }

    public void submitQuestion(long timer) {
        waitVisibilityOfElement(timer, SUBMIT_QUESTION_BTN);
        findElementByXpath(SUBMIT_QUESTION_BTN).click();
    }
}
