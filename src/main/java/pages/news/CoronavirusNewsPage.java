package pages.news;

import pages.main.BasePage;
import org.openqa.selenium.WebDriver;

public class CoronavirusNewsPage extends BasePage {

    private static final String HAVE_YOUR_SAY_NEWS_LINK = "//nav[contains(@class, 'wide')]//a[contains(@href, 'your_say')]";


    public CoronavirusNewsPage(WebDriver driver) {
        super(driver);
    }


    public void goToHaveYourSayNewsPage(long timer) {
        waitVisibilityOfElement(timer, HAVE_YOUR_SAY_NEWS_LINK);
        findElementByXpath(HAVE_YOUR_SAY_NEWS_LINK).click();
    }
}
