package pages.news;

import pages.main.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MainNewsPage extends BasePage {

    private static final String TOP_NEWS_TITLE = "//div[contains(@class, 'inline-block@m')]//h3";
    private static final String SECONDARY_NEWS_TITLE = "//div[contains(@class, 'top-stories__secondary-item')]//h3";
    private static final String HEADLINE_ARTICLE_CATEGORY= "//div[contains(@class, 'inline-block@m')]//a[contains(@class, 'section-link')]//span";
    private static final String SEARCH_LINK = "//a[contains(@class, 'search')]";
    private static final String CORONAVIRUS_NEWS_LINK = "//nav[contains(@class, 'wide')]//a[contains(@href, 'coronavirus')]";


    public MainNewsPage(WebDriver driver) {
        super(driver);
    }


    public void waitVisibilityOfTopNewsTitle(long timer) {
        waitVisibilityOfElement(timer, TOP_NEWS_TITLE);
    }

    public String getTopNewsTitleText() {
        return findElementByXpath(TOP_NEWS_TITLE).getText();
    }

    public List<WebElement> getSecondaryNewsTitles(long timer) {
        waitVisibilityOfTopNewsTitle(timer);
        return driver.findElements(By.xpath(SECONDARY_NEWS_TITLE));
    }

    public String getHeadlineArticleCategory() {
        return driver.findElement(By.xpath(HEADLINE_ARTICLE_CATEGORY)).getText();
    }

    public void goToSearchPage(long timer) {
        waitVisibilityOfElement(timer, SEARCH_LINK);
        driver.findElement(By.xpath(SEARCH_LINK)).click();
    }

    public void goToCoronavirusNewsPage(long timer) {
        waitVisibilityOfElement(timer, CORONAVIRUS_NEWS_LINK);
        findElementByXpath(CORONAVIRUS_NEWS_LINK).click();
    }
}
