package pages.news;

import pages.main.BasePage;
import org.openqa.selenium.WebDriver;
import pages.utils.QuestionFormData;

public class AskQuestionNewsPage extends BasePage {

    private static final String CLOSE_REGISTRATION_MODAL_BTN = "//button[contains(@class, 'close')]";
    QuestionForm questionForm;


    public AskQuestionNewsPage(WebDriver driver) {
        super(driver);
        this.questionForm = new QuestionForm(driver);
    }


    public void closeRegistrationModal(long timer) {
        waitVisibilityOfElement(timer, CLOSE_REGISTRATION_MODAL_BTN);
        findElementByXpath(CLOSE_REGISTRATION_MODAL_BTN).click();
    }

    public void submitQuestionForm(long timer, QuestionFormData data) {
        questionForm.fillInTextarea(timer, data.getQuestion());
        questionForm.fillInNameField(timer, data.getName());
        questionForm.fillInEmailField(timer, data.getEmail());
        questionForm.fillInPhoneField(timer, data.getPhone());
        questionForm.fillInLocationField(timer, data.getLocation());
        questionForm.fillInAgeField(timer, data.getAge());
        questionForm.markAcceptTermsOfServiceCheckbox(timer, data.isMark());
        questionForm.submitQuestion(timer);
    }

    public String getFormSubmitErrorMsg(long timer) {
        return questionForm.getSubmitFormErrorMsgText(timer);
    }
}
