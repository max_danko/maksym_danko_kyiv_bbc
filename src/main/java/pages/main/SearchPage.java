package pages.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchPage extends BasePage {

    private static final String SEARCH_INPUT = "//input[@id='search-input']";
    private static final String SUBMIT_SEARCH_BTN = "//button[@type='submit']";
    private static final String SEARCH_RESULTS_TITLES = "//a[contains(@class, 'ssrcss-1ynlzyd')]//p//span";


    public SearchPage(WebDriver driver) {
        super(driver);
    }


    private void waitVisibilityOfSearchInput(long timer) {
        waitVisibilityOfElement(timer, SEARCH_INPUT);
    }

    private WebElement getSearchInput() {
        return findElementByXpath(SEARCH_INPUT);
    }

    private WebElement getSearchSubmitBtn() {
        return findElementByXpath(SUBMIT_SEARCH_BTN);
    }

    public void makeSearch(long timer, String searchText) {
        waitVisibilityOfSearchInput(timer);
        getSearchInput().sendKeys(searchText);
        getSearchSubmitBtn().click();
    }

    private void waitVisibilityOfSearchResultsTitles(long timer) {
        waitVisibilityOfElements(timer, SEARCH_RESULTS_TITLES);
    }

    private List<WebElement> getSearchResultsTitles() {
        return driver.findElements(By.xpath(SEARCH_RESULTS_TITLES));
    }

    public String getFirstSearchResultsTitleText(long timer) {
        waitVisibilityOfSearchResultsTitles(timer);
        return getSearchResultsTitles().get(0).getText();
    }

}
