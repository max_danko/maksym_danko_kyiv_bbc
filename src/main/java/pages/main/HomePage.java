package pages.main;

import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

    private static final String NEWS_LINK = "//nav[contains(@class,'international')]//a[contains(@href, 'news')]";
    private static final String SPORT_LINK = "//nav[contains(@class,'international')]//a[contains(@href, 'sport')]";

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    private void waitVisibilityOfNewsLink(long timer) {
        waitVisibilityOfElement(timer, NEWS_LINK);
    }
    private void waitVisibilityOfSportLink(long timer) {
        waitVisibilityOfElement(timer, SPORT_LINK);
    }

    private void clickOnNewsLink() {
        findElementByXpath(NEWS_LINK).click();
    }

    private void clickOnSportsLink() {
        findElementByXpath(SPORT_LINK).click();
    }

    public void goToNewsPage(long timer) {
        waitVisibilityOfNewsLink(timer);
        clickOnNewsLink();
    }

    public void goToSportPage(long timer) {
        waitVisibilityOfSportLink(timer);
        clickOnSportsLink();
    }
}
