package manager;

import org.openqa.selenium.WebDriver;
import pages.main.HomePage;
import pages.main.SearchPage;
import pages.news.*;
import pages.sports.FootballSportPage;
import pages.sports.MainSportPage;
import pages.sports.MatchFootballSportPage;
import pages.sports.ScoresFootballSportPage;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }


    public MainNewsPage getNewsPage() {
        return new MainNewsPage(getDriver());
    }

    public CoronavirusNewsPage getCoronavirusNewsPage() {
        return new CoronavirusNewsPage(getDriver());
    }

    public HaveYourSayNewsPage getHaveYourSayNewsPage() {
        return new HaveYourSayNewsPage(getDriver());
    }

    public AskQuestionNewsPage getAskQuestionNewsPage() {
        return new AskQuestionNewsPage(getDriver());
    }

    public SearchPage getSearchPage() {
        return new SearchPage(getDriver());
    }

    public QuestionForm getQuestionForm() {
        return new QuestionForm(getDriver());
    }


    public MainSportPage getSportPage() {
        return new MainSportPage(getDriver());
    }

    public FootballSportPage getFootballSportPage() {
        return new FootballSportPage(getDriver());
    }

    public ScoresFootballSportPage getScoresFootballSportPage() {
        return new ScoresFootballSportPage(getDriver());
    }

    public MatchFootballSportPage getMatchFootballSportPage() {
        return new MatchFootballSportPage(getDriver());
    }
}
