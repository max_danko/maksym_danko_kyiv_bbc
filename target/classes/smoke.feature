Feature: Smoke
  As a user
  I want to test BBC site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check top news article title
    Given User opens '<homePage>' page
    When User clicks News page link
    And User waits for top news title on News page
    Then User checks that top news title is '<topNewsTitle>'

    Examples:
      | homePage             | topNewsTitle                                   |
      | https://www.bbc.com/ | US to review response to Texas school shooting |


  Scenario Outline: Check secondary news article titles
    Given User opens '<homePage>' page
    When User clicks News page link
    And User waits for top news title on News page
    Then User checks that secondary news titles are equal to expected titles

    Examples:
      | homePage            |
      |https://www.bbc.com/ |


  Scenario Outline: Check search result for category headline article
    Given User opens '<homePage>' page
    When User clicks News page link
    And User saves top article category from News page
    When User clicks on search field
    And User makes search by top article category from News page
    Then User checks that top news title from search results is '<topSearchedTitle>'

    Examples:
      | homePage             | topSearchedTitle                                                 |
      | https://www.bbc.com/ | Monkeypox cases investigated in Europe, US, Canada and Australia |


  Scenario Outline: Check submission error text when submitting incomplete form data
    Given User opens '<homePage>' page
    When User clicks News page link
    And User clicks Coronavirus link
    And User clicks 'Your Coronavirus Stories' tab
    And User clicks 'Your questions answered: What questions do you have?' link
    When Registration modal shows up user clicks on 'Close' button
    And User enters '<question>' in 'Question' teaxtarea
    And User enters '<name>' in 'Name' input field
    And User enters '<email>' in 'Email' input field
    And User enters '<phone>' in 'Phone' input field
    And User enters '<location>' in 'Location' input field
    And User enters '<age>' in 'Age' input field
    And User marks '<checkmark>' 'I accept the Terms of Service' checkmark field
    And User clicks 'Submit' button
    Then User checks submission error '<submissionError>'

    Examples:
      | homePage             | question                      | name   | email                  | phone         | location | age | checkmark | submissionError               |
      | https://www.bbc.com/ | Is coronavirus really deadly? | Maksym |                        | +380632591717 | London   | 87  | true      | Email address can\'t be blank |
      | https://www.bbc.com/ | Is coronavirus really deadly? | Maksym | maxgalante20@gmail.com | +380632591717 | London   | 87  | false     | must be accepted              |
      | https://www.bbc.com/ |                               | Maksym | maxgalante20@gmail.com | +380632591717 | London   | 87  | true      | can\'t be blank               |


  Scenario Outline: Check sport
    Given User opens '<homePage>' page
    When User clicks Sport page link
    And User clicks Football tab
    And User clicks 'Scores and Fixtures' link
    When User searching for '<championship>'
    And User selects '<month>'
    Then User checks that '<firstTeam>' vs '<secondTeam>' have played with score '<firstTeamScore>' : '<secondTeamScore>'
    When User clicks on '<firstTeam>' vs '<secondTeam>' match
    Then User checks page has same teams '<firstTeam>' vs '<secondTeam>' with same score '<firstTeamScore>' : '<secondTeamScore>'

    Examples:
      | homePage             | championship          | month | firstTeam      | secondTeam      | firstTeamScore | secondTeamScore |
      | https://www.bbc.com/ | Scottish Championship | APR   | Arbroath       | Greenock Morton | 3              | 0               |
      | https://www.bbc.com/ | Champions League      | MAR   | Juventus       | Villarreal      | 0              | 3               |
      | https://www.bbc.com/ | National League       | FEB   | Woking         | Notts County    | 0              | 2               |
      | https://www.bbc.com/ | League Two            | FEB   | Newport County | Tranmere Rovers | 4              | 2               |
      | https://www.bbc.com/ | National League       | APR   | Bromley        | Chesterfield    | 4              | 2               |
